#  Variables.tf declares has the default variables that are shared by all environments
# $var.region, $var.domain, $var.tf_s3_bucket


# Read credentials from environment variables
#$ export AWS_ACCESS_KEY_ID="anaccesskey"
#$ export AWS_SECRET_ACCESS_KEY="asecretkey"
#$ export AWS_DEFAULT_REGION="us-west-2"
#$ terraform plan
provider "aws" {
  profile = "${var.aws_profile}"
  region  = "${var.region}"
}

provider "aws" {
  profile = "${var.aws_profile}"
  region  = "us-east-1"
  alias   = "us-east-1"
}

provider "aws" {
  profile = "${var.aws_profile}"
  region  = "us-west-1"
  alias   = "us-west-1"
}



data "terraform_remote_state" "master_state" {
  backend = "s3"
  config {
    bucket = "${var.tf_s3_bucket}"
    region = "${var.region}"
    key    = "${var.master_state_file}"
  }
}

variable "aws_profile" {
  description = "Which AWS profile is should be used? Defaults to \"default\""
  default     = "default"
}
variable "region" { default = "us-east-1" }

// Google variables
provider "gke" {
  profile = "${var.google_profile}"
  region = "${var.gke_region}"
  credentials = "${var.google_credentials}"
}

provider "gcp" {
  profile = "${var.google_profile}"
  region = "${var.gcp_region}"
  credentials = "${var.google_credentials}"
}

variable "google_profile" {
  description = "Which Google profile is should be used? Defaults to \"default\""
  default = "default"
}

variable "gcp_region " {
  description = "Which Google region should be used for GCP?"
}

variable "gke_region" {
  description = "Which Google region should be used for GKE?"
}
variable "google_credentials" {
  description = "Path to file with the secret for Google Cloud"
  default = "${file("/Users/bpabon/.ssh/boink-cluster.json")}"

# This should be changed to reflect the service / stack defined by this repo
# for example replace "ref" with "cms", "slackbot", etc
variable "stack" { default = "ref" }

variable "tf_s3_bucket" {
  description = "S3 bucket Terraform can use for state"
  default     = "rk-devops-state-us-east-1"
}

variable "master_state_file" { default = "boinc-cloud/state/base/base.tfstate" }
variable "prod_state_file" { default = "boinc-cloud/state/production/production.tfstate" }
variable "staging_state_file" { default = "boinc-cloud/state/staging/staging.tfstate" }
variable "dev_state_file" { default = "boinc-cloud/state/dev/dev.tfstate" }
